﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAndLINQ.Configuration
{
    public static class ApiUris
    {
        public const string BaseAddress = "https://bsa-dotnet.azurewebsites.net/api/";

        public const string Projects = "projects";
        public const string Tasks = "tasks";
        public const string Teams = "teams";
        public const string Users = "users";
    }
}
