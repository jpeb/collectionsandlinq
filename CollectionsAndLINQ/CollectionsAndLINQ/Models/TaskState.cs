namespace CollectionsAndLINQ.Models
{
    public enum TaskState
    {
        Created = 0,
        Canceled = 1,
        Finished = 2,
        InProgress = 3,
    }
}
