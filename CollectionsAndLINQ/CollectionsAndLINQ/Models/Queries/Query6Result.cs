﻿namespace CollectionsAndLINQ.Models.Queries
{
    public class Query6Result
    {
        public User User { get; set; }
        public Project LastUserProject { get; set; }
        public int LastProjectTasksNumber { get; set; }
        public int UnfinishedTasksNumber { get; set; }
        public Task LongestTask { get; set; }
    }
}
