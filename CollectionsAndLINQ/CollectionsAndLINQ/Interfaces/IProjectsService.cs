﻿using CollectionsAndLINQ.Models;
using CollectionsAndLINQ.Models.Queries;
using System;
using System.Collections.Generic;

namespace CollectionsAndLINQ.Interfaces
{
    public interface IProjectsService : IDisposable
    {
        ICollection<Project> Projects { get; }
        ICollection<Task> Tasks { get; }
        ICollection<Team> Teams { get; }
        ICollection<User> Users { get; }

        System.Threading.Tasks.Task LoadData();


        // Query 1
        Dictionary<Project, int> GetUserProjectTasksNumber(int userId);

        // Query 2
        ICollection<Task> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength);

        // Query 3
        ICollection<(int id, string name)> GetUserTasksFinishedInYear(int userId, int finishedYear);

        // Query 4
        ICollection<(int id, string name, List<User> users)> GetTeamsWithUsersOlderThan(int minYearsNumber);

        // Query 5
        ICollection<(User user, List<Task> tasks)> GetUsersWithTasks();

        // Query 6
        Query6Result GetUserTaskStatictics(int userId);

        // Query 7
        ICollection<Query7Result> GetProjectStatictics(int minProjectDescriptionLengthForCounting, int maxProjectTasksCountForCounting);
    }
}