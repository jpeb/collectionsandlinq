﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAndLINQ.Menu
{
    public class ConsoleMenu
    {
        public ConsoleMenu(List<MenuOption> options, string title)
        {
            MenuOptions = options;
            Title = title;
        }

        public List<MenuOption> MenuOptions { get; set; }
        public (ConsoleColor fgColor, ConsoleColor bgColor) TitleColor { get; set; }
        public (ConsoleColor fgColor, ConsoleColor bgColor) PrimaryColor { get; set; }
        public (ConsoleColor fgColor, ConsoleColor bgColor) SelectedColor { get; set; }
        public string Title { get; set; }
        public int SelectedIndex { get; private set; }
        public int TopOffset { get; private set; }
        public int LeftOffset { get; set; } = 2;

        public void RunMenu()
        {
            SelectedIndex = 0;
            Console.Clear();
            Console.CursorVisible = false;
            DrawMenu();

            while (true)
            {
                var keyInfo = Console.ReadKey();

                switch (keyInfo.Key)
                {
                    case ConsoleKey.DownArrow:
                        {
                            Utils.WriteToPos(MenuOptions[SelectedIndex].Name, LeftOffset, TopOffset + SelectedIndex,
                                PrimaryColor);
                            SelectedIndex = SelectedIndex == MenuOptions.Count - 1 ? 0 : SelectedIndex + 1;
                            Utils.WriteToPos(MenuOptions[SelectedIndex].Name, LeftOffset, TopOffset + SelectedIndex,
                                SelectedColor);

                            break;
                        }

                    case ConsoleKey.UpArrow:
                        {
                            Utils.WriteToPos(MenuOptions[SelectedIndex].Name, LeftOffset, TopOffset + SelectedIndex,
                                PrimaryColor);
                            SelectedIndex = SelectedIndex == 0 ? MenuOptions.Count - 1 : SelectedIndex - 1;
                            Utils.WriteToPos(MenuOptions[SelectedIndex].Name, LeftOffset, TopOffset + SelectedIndex,
                                SelectedColor);

                            break;
                        }

                    case ConsoleKey.Enter:
                        {
                            Console.Clear();
                            Console.CursorVisible = true;

                            try
                            {
                                MenuOptions[SelectedIndex].Action.Invoke();
                            }
                            catch (Exception e)
                            {
                                Utils.WriteColored("\n\n            ERROR            \n", ConsoleColor.Black,
                                    ConsoleColor.DarkRed);
                                Utils.WriteColored(e.Message, ConsoleColor.DarkRed, ConsoleColor.Black);
                            }

                            Utils.WriteColored("\n\nPress any key to continue...", ConsoleColor.DarkGreen,
                                ConsoleColor.Black);
                            Console.ReadKey();
                            Console.Clear();

                            Console.CursorVisible = false;
                            DrawMenu();
                            break;
                        }
                }
            }
        }

        private void DrawMenu()
        {
            Utils.WriteColored(Title + Environment.NewLine, TitleColor);

            TopOffset = Console.CursorTop;
            for (var i = 0; i < MenuOptions.Count; i++)
            {
                Console.Write("".PadLeft(LeftOffset));
                Utils.WriteColored($"{MenuOptions[i].Name}\n",
                    i == SelectedIndex ? SelectedColor : PrimaryColor);
            }
        }
    }
}
