﻿using System;

namespace CollectionsAndLINQ.Menu
{
    public class MenuOption
    {
        public MenuOption(string name, Action action)
        {
            Name = name;
            Action = action;
        }

        public string Name { get; }
        public Action Action { get; }
    }
}
