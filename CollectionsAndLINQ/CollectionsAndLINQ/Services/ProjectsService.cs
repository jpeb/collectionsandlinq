﻿using CollectionsAndLINQ.Configuration;
using CollectionsAndLINQ.Interfaces;
using CollectionsAndLINQ.Models;
using CollectionsAndLINQ.Models.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using Task = CollectionsAndLINQ.Models.Task;

namespace CollectionsAndLINQ.Services
{
    public class ProjectsService : IDisposable, IProjectsService
    {
        public ICollection<Project> Projects { get; private set; }
        public ICollection<User> Users { get; private set; }
        public ICollection<Team> Teams { get; private set; }
        public ICollection<Task> Tasks { get; private set; }

        private readonly HttpClient _httpClient;

        public ProjectsService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        // Load data
        public async System.Threading.Tasks.Task LoadData()
        {
            var projects = await _httpClient.GetFromJsonAsync<ICollection<Project>>(ApiUris.Projects);
            var users = await _httpClient.GetFromJsonAsync<ICollection<User>>(ApiUris.Users);
            var teams = await _httpClient.GetFromJsonAsync<ICollection<Team>>(ApiUris.Teams);
            var tasks = await _httpClient.GetFromJsonAsync<ICollection<Task>>(ApiUris.Tasks);

            // join tasks with users
            tasks = tasks.Join(users, t => t.PerformerId, u => u.Id, (task, user) =>
            {
                task.Performer = user;
                return task;
            }).ToList();

            // join projects with users
            projects = projects.Join(users, p => p.AuthorId, u => u.Id, (project, user) =>
            {
                project.Author = user;
                return project;
            }).ToList();

            // join projects with teams
            projects = projects.Join(teams, p => p.TeamId, t => t.Id, (project, team) =>
            {
                project.Team = team;
                return project;
            }).ToList();

            // join projects with tasks
            projects = projects.GroupJoin(tasks, p => p.Id, t => t.ProjectId, (project, tasks) =>
            {
                project.Tasks = tasks.ToList();
                return project;
            }).ToList();

            Projects = projects;
            Users = users;
            Teams = teams;
            Tasks = tasks;
        }

        // Query 1
        public Dictionary<Project, int> GetUserProjectTasksNumber(int userId)
        {
            return Projects
                    .Where(p => p.Author.Id == userId)
                    .ToDictionary(p => p, pr => pr.Tasks.Count);
        }

        // Query 2
        public ICollection<Task> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength)
        {
            return Tasks
                    .Where(t => t.Performer.Id == userId && t.Name.Length < maxTaskNameLength)
                    .ToList();
        }

        // Query 3
        public ICollection<(int id, string name)> GetUserTasksFinishedInYear(int userId, int finishedYear)
        {
            return Tasks
                    .Where(t => t.Performer.Id == userId &&
                                t.FinishedAt.HasValue &&
                                t.FinishedAt.Value.Year == finishedYear)
                    .Select(t => (t.Id, t.Name))
                    .ToList();
        }

        // Query 4
        public ICollection<(int id, string name, List<User> users)> GetTeamsWithUsersOlderThan(int minYearsNumber)
        {
            var currentYear = DateTime.Now.Year;

            return Users
                    .Where(u => u.TeamId != null && currentYear - u.BirthDay.Year > minYearsNumber)
                    .GroupBy(u => u.TeamId)
                    .Join(Teams, g => g.Key, t => t.Id,
                        (group, team) => (group.Key ?? -1, team.Name, group.OrderByDescending(u => u.RegisteredAt).ToList()))
                    .ToList();
        }

        // Query 5
        public ICollection<(User user, List<Task> tasks)> GetUsersWithTasks()
        {
            return Tasks
                    .GroupBy(t => t.Performer)
                    .OrderBy(g => g.Key.FirstName)
                    .Select(group => (group.Key, group.OrderByDescending(t => t.Name.Length).ToList()))
                    .ToList();
        }

        // Query6
        public Query6Result GetUserTaskStatictics(int userId)
        {
            var result = from task in Tasks
                         where task.Performer.Id == userId
                         group task by task.Performer into g
                         let sortedProjects = Projects.OrderByDescending(p => p.CreatedAt)
                         let lastProject = sortedProjects.First()
                         let lastUserProject = sortedProjects.FirstOrDefault(p => p.Author.Id == userId)
                         select new Query6Result
                         {
                             User = g.Key,
                             LastUserProject = lastUserProject,
                             LastProjectTasksNumber = lastProject.Tasks.Count,
                             UnfinishedTasksNumber = g.Count(t => t.State != TaskState.Finished),
                             LongestTask = g.OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault()
                         };

            return result.FirstOrDefault();
        }

        // Query 7
        public ICollection<Query7Result> GetProjectStatictics(int minProjectDescriptionLengthForCounting, int maxProjectTasksCountForCounting)
        {
            return Projects
               .Join(Teams, p => p.Team.Id, t => t.Id, (p, t) => new { Project = p, Team = t })
               .GroupJoin(Users, pt => pt.Team.Id, u => u.TeamId, (pt, u) =>
               {
                   return new Query7Result
                   {
                       Project = pt.Project,
                       LongestTaskByDescription = pt.Project.Tasks.Any()
                           ? pt.Project.Tasks.Aggregate((t1, t2) => t1.Description.Length > t2.Description.Length ? t1 : t2)
                           : null,
                       ShortestTaskByName = pt.Project.Tasks.Any()
                           ? pt.Project.Tasks.Aggregate((t1, t2) => t1.Name.Length > t2.Name.Length ? t2 : t1)
                           : null,
                       UsersNumber = pt.Project.Description.Length > minProjectDescriptionLengthForCounting
                       || pt.Project.Tasks.Count < maxProjectTasksCountForCounting
                       ? u.Count()
                       : null,
                   };
               }).ToList();
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}