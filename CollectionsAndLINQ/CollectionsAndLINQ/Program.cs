﻿using CollectionsAndLINQ.Configuration;
using CollectionsAndLINQ.Interfaces;
using CollectionsAndLINQ.Menu;
using CollectionsAndLINQ.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CollectionsAndLINQ
{
    internal class Program
    {
        private const string Title = @"
   ____      _ _           _   _                    _              _ _     ___ _   _  ___  
  / ___|___ | | | ___  ___| |_(_) ___  _ __  ___   / \   _ __   __| | |   |_ _| \ | |/ _ \ 
 | |   / _ \| | |/ _ \/ __| __| |/ _ \| '_ \/ __| / _ \ | '_ \ / _` | |    | ||  \| | | | |
 | |__| (_) | | |  __/ (__| |_| | (_) | | | \__ \/ ___ \| | | | (_| | |___ | || |\  | |_| |
  \____\___/|_|_|\___|\___|\__|_|\___/|_| |_|___/_/   \_\_| |_|\__,_|_____|___|_| \_|\__\_\

    Use arrows and Enter for navigation...                                                                                          
";

        private static async Task Main(string[] args)
        {
            Console.Title = "CollectionsAndLINQ";
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            var client = new HttpClient
            {
                BaseAddress = new Uri(ApiUris.BaseAddress)
            };

            IProjectsService projectsService = new ProjectsService(client);

            await projectsService.LoadData();

            var app = new AppController(
                projectsService
            );

            List<MenuOption> options = new()
            {
                new MenuOption("Query 1 - Кол-во тасков у проекта пользователя", app.Query1),
                new MenuOption("Query 2 - Список тасков пользователя с name < X", app.Query2),
                new MenuOption("Query 3 - Список выполненых тасков пользователя за X год", app.Query3),
                new MenuOption("Query 4 - Команды пользователей с участниками старше X лет", app.Query4),
                new MenuOption("Query 5 - Список пользователей и их таски", app.Query5),
                new MenuOption("Query 6 - Информация о тасках пользователя", app.Query6),
                new MenuOption("Query 7 - Информация о тасках проекта", app.Query7),
                new MenuOption("Exit", () =>
                {
                    app.Dispose();
                    Environment.Exit(0);
                })
            };

            var menu = new ConsoleMenu(options, Title)
            {
                PrimaryColor = (ConsoleColor.White, ConsoleColor.Black),
                TitleColor = (ConsoleColor.Red, ConsoleColor.Black),
                SelectedColor = (ConsoleColor.Black, ConsoleColor.DarkGreen)
            };

            menu.RunMenu();
        }
    }
}