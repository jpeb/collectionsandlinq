﻿using System;

namespace CollectionsAndLINQ
{
    public static class Utils
    {
        public static void WriteColored(string str, ConsoleColor? fgColor = null, ConsoleColor? bgColor = null)
        {
            var (oldFg, oldBg) = (Console.ForegroundColor, Console.BackgroundColor);
            Console.ForegroundColor = fgColor ?? oldFg;
            Console.BackgroundColor = bgColor ?? oldBg;
            Console.Write(str);
            Console.ForegroundColor = oldFg;
            Console.BackgroundColor = oldBg;
        }

        public static void WriteColored(string str, (ConsoleColor? fgColor, ConsoleColor? bgColor) color)
            => WriteColored(str, color.fgColor, color.bgColor);

        public static void WriteToPos(string str, int left, int top, ConsoleColor? fgColor = null, ConsoleColor? bgColor = null)
        {
            Console.SetCursorPosition(left, top);
            WriteColored(str, fgColor, bgColor);
        }

        public static void WriteToPos(string str, int left, int top,
            (ConsoleColor? fgColor, ConsoleColor? bgColor) color)
            => WriteToPos(str, left, top, color.fgColor, color.bgColor);
    }
}
