﻿using CollectionsAndLINQ.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAndLINQ
{
    internal class AppController : IDisposable
    {
        private readonly IProjectsService _projectsService;

        public AppController(IProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        public void Query1()
        {
            Console.Write("\nEnter user id: ");

            int userId;
            bool idValid;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out userId);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.",ConsoleColor.DarkRed);
            } while (!idValid);

            var result = _projectsService.GetUserProjectTasksNumber(userId);

            if(result.Count == 0)
            {
                Utils.WriteColored($"\n\nUser with id={userId} not found or doesn't have a project\n", ConsoleColor.Red);
                return;
            }

            var data = result.First();

            Console.Write("\n\nProject: ");
            Utils.WriteColored($"{data.Key.Name}\n", ConsoleColor.Green);
            Console.Write("Description: ");
            Utils.WriteColored($"{data.Key.Description}\n", ConsoleColor.Green);
            Console.Write("CreatedAt: ");
            Utils.WriteColored($"{data.Key.CreatedAt}\n", ConsoleColor.Green);
            Console.Write("Deadline: ");
            Utils.WriteColored($"{data.Key.Deadline}\n", ConsoleColor.Green);
            Console.Write("Author: ");
            Utils.WriteColored($"{data.Key.Author.FirstName} {data.Key.Author.LastName}(ID = {data.Key.Author.Id})\n", ConsoleColor.Green);
            Console.Write("Team: ");
            Utils.WriteColored($"{data.Key.Team.Name}(ID = {data.Key.Team.Id})\n", ConsoleColor.Green);

            Console.Write("\n► Tasks number: ");
            Utils.WriteColored($"{data.Value}\n", ConsoleColor.DarkCyan);
        }

        public void Query2()
        {
            Console.Write("\nEnter user id: ");

            int userId;
            bool idValid;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out userId);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            Console.Write("\nEnter max task name length: ");

            int maxNameLength;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out maxNameLength);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            var result = _projectsService.GetUserTasksWithNameLessThen(userId, maxNameLength);

            if (result.Count == 0)
            {
                Utils.WriteColored("\n\nNothing found:(\n", ConsoleColor.Red);
                return;
            }

            Console.WriteLine($"\n\n► User(ID = {userId}) tasks with name length less than {maxNameLength}:\n");
            int number = 1;
            foreach (var task in result)
            {
                Utils.WriteColored($" {number++}", ConsoleColor.Green);
                Console.Write(" | Name: ");
                Utils.WriteColored(task.Name, ConsoleColor.Green);
                Console.Write(" | State: ");
                Utils.WriteColored(task.State.ToString(), ConsoleColor.Green);
                Console.Write(" | CreatedAt: ");
                Utils.WriteColored(task.CreatedAt.ToString(), ConsoleColor.Green);
                Console.Write(" | FinishedAt: ");
                Utils.WriteColored(task?.FinishedAt?.ToString() ?? "Not finished", ConsoleColor.Green);
                Console.Write("\n Description: ");
                Utils.WriteColored($"{task.Description}\n\n", ConsoleColor.Green);
            }
        }

        public void Query3()
        {
            Console.Write("\nEnter user id: ");

            int userId;
            bool idValid;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out userId);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            Console.Write("\nEnter year: ");

            int year;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out year);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            var result = _projectsService.GetUserTasksFinishedInYear(userId, year);

            if (result.Count == 0)
            {
                Utils.WriteColored("\n\nNothing found:(\n", ConsoleColor.Red);
                return;
            }

            Console.WriteLine($"\n\n► User(ID = {userId}) tasks finished in {year}:\n");
            int number = 1;
            foreach (var task in result)
            {
                Utils.WriteColored($" {number++}", ConsoleColor.Green);
                Console.Write(" | ID: ");
                Utils.WriteColored(task.id.ToString(), ConsoleColor.Green);
                Console.Write(" | Name: ");
                Utils.WriteColored(task.name, ConsoleColor.Green);
            }
        }

        public void Query4()
        {
            Console.Write("\nEnter min user age in years: ");

            int year;
            bool idValid;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out year);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            var result = _projectsService.GetTeamsWithUsersOlderThan(year);

            if (result.Count == 0)
            {
                Utils.WriteColored("\n\nNothing found:(\n", ConsoleColor.Red);
                return;
            }

            Console.WriteLine($"\n\n► Teams and their users:");
            int number = 1;
            var currentDate = DateTime.Now;
            foreach (var team in result)
            {
                Utils.WriteColored($"\n\n Team {number++}", ConsoleColor.Green);
                Console.Write(" | ID: ");
                Utils.WriteColored(team.id.ToString(), ConsoleColor.Green);
                Console.Write(" | Name: ");
                Utils.WriteColored($"{team.name}\n", ConsoleColor.Green);
                
                foreach (var user in team.users)
                    Console.WriteLine($"  {user.FirstName} {user.LastName} | {user.Email} | BirthDay: {user.BirthDay.ToShortDateString()} | Age: {(currentDate.Year - user.BirthDay.Year)}");
                
            }
        }

        public void Query5()
        {

            var result = _projectsService.GetUsersWithTasks();

            if (result.Count == 0)
            {
                Utils.WriteColored("\n\nNothing found:(\n", ConsoleColor.Red);
                return;
            }

            Console.WriteLine($"\n\n► Users and their tasks:");
            foreach (var userTasks in result)
            {
                Utils.WriteColored($"\n\n► User {userTasks.user.Id}", ConsoleColor.Blue);
                Console.Write(" | ");
                Utils.WriteColored($"{userTasks.user.FirstName} {userTasks.user.LastName}", ConsoleColor.Green);
                Console.Write(" | ");
                Utils.WriteColored(userTasks.user.Email, ConsoleColor.Green);
                Console.Write(" | BirthDay: ");
                Utils.WriteColored(userTasks.user.BirthDay.ToShortDateString(), ConsoleColor.Green);
                Console.Write(" | RegisteredAt: ");
                Utils.WriteColored($"{userTasks.user.RegisteredAt.ToShortDateString()}\n\n", ConsoleColor.Green);
                Utils.WriteColored("  TASKS:\n\n", ConsoleColor.Cyan);

                foreach (var task in userTasks.tasks)
                {
                    Console.Write("  ID: ");
                    Utils.WriteColored(task.Id.ToString(), ConsoleColor.Green);
                    Console.Write(" | Name: ");
                    Utils.WriteColored(task.Name, ConsoleColor.Green);
                    Console.Write(" | State: ");
                    Utils.WriteColored(task.State.ToString(), ConsoleColor.Green);
                    Console.Write(" | CreatedAt: ");
                    Utils.WriteColored(task.CreatedAt.ToShortDateString(), ConsoleColor.Green);
                    Console.Write(" | FinishedAt: ");
                    Utils.WriteColored(task?.FinishedAt?.ToShortDateString() ?? "Not finished", ConsoleColor.Green);
                    Console.Write("\n  Description: ");
                    Utils.WriteColored($"{task.Description}\n\n", ConsoleColor.Green);
                }
            }
        }

        public void Query6()
        {
            Console.Write("\nEnter user id: ");

            int userId;
            bool idValid;
            do
            {
                Console.Write("\n > ");
                idValid = int.TryParse(Console.ReadLine(), out userId);

                if (!idValid)
                    Utils.WriteColored("\nAn error occurred. Please try again.", ConsoleColor.DarkRed);
            } while (!idValid);

            var result = _projectsService.GetUserTaskStatictics(userId);

            if (result is null)
            {
                Utils.WriteColored($"\n\nUser with id={userId} not found\n", ConsoleColor.Red);
                return;
            }

            Utils.WriteColored(" User:\n", ConsoleColor.Cyan);
            Console.Write(" ID: ");
            Utils.WriteColored($"{result.User.Id}", ConsoleColor.Green);
            Console.Write(" | Name: ");
            Utils.WriteColored($" {result.User.FirstName} {result.User.LastName}", ConsoleColor.Green);
            Console.Write(" | Email: ");
            Utils.WriteColored($" {result.User.Email}\n", ConsoleColor.Green);

            if (result.LastUserProject is null)
            {
                Utils.WriteColored($"  User doesn't have project", ConsoleColor.Red);
                return;
            }

            Utils.WriteColored("\n Last user project:\n", ConsoleColor.Cyan);
            Console.Write(" ID: ");
            Utils.WriteColored($"{result.LastUserProject.Id}", ConsoleColor.Green);
            Console.Write(" | Name: ");
            Utils.WriteColored($"{result.LastUserProject.Name}", ConsoleColor.Green);
            Console.Write(" | CreatedAt: ");
            Utils.WriteColored($"{result.LastUserProject.CreatedAt}", ConsoleColor.Green);
            Console.Write(" | Deadline: ");
            Utils.WriteColored($"{result.LastUserProject.Deadline}\n", ConsoleColor.Green);
            Console.Write(" Description: ");
            Utils.WriteColored($"{result.LastUserProject.Description}\n", ConsoleColor.Green);

            Utils.WriteColored("\n Last users project tasks number: ", ConsoleColor.Cyan);
            Utils.WriteColored($"{result.LastProjectTasksNumber}\n", ConsoleColor.Green);

            Utils.WriteColored("\n All user unfinished tasks number: ", ConsoleColor.Cyan);
            Utils.WriteColored($"{result.UnfinishedTasksNumber}\n", ConsoleColor.Green);

            Utils.WriteColored("\n User longest task:\n", ConsoleColor.Cyan);
            if(result.LongestTask is null)
            {
                Utils.WriteColored($"  User doesn't have tasks", ConsoleColor.Red);
                return;
            }
            Console.Write(" ID: ");
            Utils.WriteColored($"{result.LongestTask.Id}", ConsoleColor.Green);
            Console.Write(" | Name: ");
            Utils.WriteColored(result.LongestTask.Name, ConsoleColor.Green);
            Console.Write(" | State: ");
            Utils.WriteColored(result.LongestTask.State.ToString(), ConsoleColor.Green);
            Console.Write(" | CreatedAt: ");
            Utils.WriteColored(result.LongestTask.CreatedAt.ToString(), ConsoleColor.Green);
            Console.Write(" | FinishedAt: ");
            Utils.WriteColored(result.LongestTask?.FinishedAt?.ToString() ?? "Not finished", ConsoleColor.Green);
            Console.Write("\n Description: ");
            Utils.WriteColored($"{result.LongestTask.Description}\n\n", ConsoleColor.Green);
        }

        public void Query7()
        {
            var result = _projectsService.GetProjectStatictics(20, 3);

            if (result.Count == 0)
            {
                Utils.WriteColored("\n\nNothing found:(\n", ConsoleColor.Red);
                return;
            }

            Console.WriteLine($"\n\n► Projects info:");
            foreach (var projectInfo in result)
            {
                Utils.WriteColored($"\n\n► Project {projectInfo.Project.Id}", ConsoleColor.Blue);
                Console.Write(" | ");
                Utils.WriteColored($"{projectInfo.Project.Name}", ConsoleColor.Green);
                Console.Write(" | Author: ");
                Utils.WriteColored($"{projectInfo.Project.Author.FirstName} {projectInfo.Project.Author.LastName}", ConsoleColor.Green);
                Console.Write(" | CreatedAt: ");
                Utils.WriteColored(projectInfo.Project.CreatedAt.ToShortDateString(), ConsoleColor.Green);
                Console.Write(" | Deadline: ");
                Utils.WriteColored(projectInfo.Project.Deadline.ToShortDateString(), ConsoleColor.Green);
                Console.Write("\n  Team: ");
                Utils.WriteColored($"{projectInfo.Project.Team.Name}(ID = {projectInfo.Project.Team.Id})", ConsoleColor.Green);
                Console.Write(" | Description: ");
                Utils.WriteColored($"{projectInfo.Project.Description}\n\n", ConsoleColor.Green);

                if (projectInfo.LongestTaskByDescription is null)
                {
                    Utils.WriteColored($"  Project doesn't have tasks", ConsoleColor.Red);
                    continue;
                }

                Utils.WriteColored("\n Longest task by description: ", ConsoleColor.Cyan);
                Console.Write("\n  ID: ");
                Utils.WriteColored(projectInfo.LongestTaskByDescription.Id.ToString(), ConsoleColor.Green);
                Console.Write(" | Name: ");
                Utils.WriteColored(projectInfo.LongestTaskByDescription.Name, ConsoleColor.Green);
                Console.Write(" | State: ");
                Utils.WriteColored(projectInfo.LongestTaskByDescription.State.ToString(), ConsoleColor.Green);
                Console.Write(" | CreatedAt: ");
                Utils.WriteColored(projectInfo.LongestTaskByDescription.CreatedAt.ToShortDateString(), ConsoleColor.Green);
                Console.Write(" | FinishedAt: ");
                Utils.WriteColored(projectInfo.LongestTaskByDescription?.FinishedAt?.ToShortDateString() ?? "Not finished", ConsoleColor.Green);
                Console.Write("\n  Description: ");
                Utils.WriteColored($"{projectInfo.LongestTaskByDescription.Description}\n", ConsoleColor.Green);

                Utils.WriteColored("\n Shortest task by name: ", ConsoleColor.Cyan);
                Console.Write("\n  ID: ");
                Utils.WriteColored(projectInfo.ShortestTaskByName.Id.ToString(), ConsoleColor.Green);
                Console.Write(" | Name: ");
                Utils.WriteColored(projectInfo.ShortestTaskByName.Name, ConsoleColor.Green);
                Console.Write(" | State: ");
                Utils.WriteColored(projectInfo.ShortestTaskByName.State.ToString(), ConsoleColor.Green);
                Console.Write(" | CreatedAt: ");
                Utils.WriteColored(projectInfo.ShortestTaskByName.CreatedAt.ToShortDateString(), ConsoleColor.Green);
                Console.Write(" | FinishedAt: ");
                Utils.WriteColored(projectInfo.ShortestTaskByName?.FinishedAt?.ToShortDateString() ?? "Not finished", ConsoleColor.Green);
                Console.Write("\n  Description: ");
                Utils.WriteColored($"{projectInfo.ShortestTaskByName.Description}\n", ConsoleColor.Green);

                Utils.WriteColored("\n Number of users in team: ", ConsoleColor.Cyan);
                Utils.WriteColored($"{(projectInfo.UsersNumber is null ? "Not calculated by condition" : projectInfo.UsersNumber)}\n", ConsoleColor.Green);
            }
        }

        public void NotImplemented()
        {
            Console.WriteLine("Cool UI under construction...");
        }

        public void Dispose()
        {
            _projectsService.Dispose();
        }
    }
}
